package com.service.zcsgl2.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-01-09T09:20:18.918Z")

@RestSchema(schemaId = "zcsgl2")
@RequestMapping(path = "/zcsgl2", produces = MediaType.APPLICATION_JSON)
public class Zcsgl2Impl {

    @Autowired
    private Zcsgl2Delegate userZcsgl2Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userZcsgl2Delegate.helloworld(name);
    }

}
